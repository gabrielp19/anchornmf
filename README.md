# BRENT: Bootstrapped Regression Effects for NMF-based Topic Models 

## TO DO 

1. Update vignette. 
2. Submit to ArXiv. 
3. Add paper code. 
4. Fix licensing. 
5. Clean up repo. 

## Basics 

This basic R package performs convex non-negative matrix factorization (NMF) with an emphasis on topic 
modelling. It "convexifies" the NMF problem by finding a set of so-called anchor words. Anchor words are words that only appear within a certain a topic, thus "defining" that topic (in a certain sense). It turns out that constructing a set of anchor words predetermines one of the two matrices that must be solved for in NMF. The remaning matrix can be found by non-negative least squares. The package also includes the option to use random projections in speeding up some of the large linear algebra calculations. This gives dramatic performance gains on large corpora. Note that, formally speaking, random projections invalidate the inference procedures described below. Thus, we recommend their use only for vanilla topic modelling without covariates. 

## Covariate Effects 

While users wishing to perform standard topic modelling may be satisfied with the above functionality alone, the main reason for BRENT's existence is its ability to incorporate document-level covariates into the topic modelling pipeline. What's more, this is done in a fast, inferentially-justifiable way. After solving the NMF problem, we pass the topic-document matrix (giving the "weight" assigned to topics in each document) to an OLS regression scheme which determines the best linear predictor of topic weight given the specified covariates. We then estimate the sampling distribution of the resulting regression coefficients (the "betas") using a fast bootstrapping algorithm. Due to the structure of NMF with anchor words, we can do this without re-computing the matrix factorization at each bootstrap iteration. In summary, we can extract confidence intervals (or any other functional of the bootstrap CDF) for the regression coefficients of interest in an efficient way. Note that none of this depends on a parametric model -- though we use tools like OLS, they are treated purely as statistical functionals of the true, underlying distribution. This is why it makes sense to estimate sampling distributions via bootstrapping. 

## Installation 

First, you'll need the `devtools` package. Next, clone this repository and set it as your working R directory. In R, run: 
```
  library(devtools)
  build() 
  install() 
```
That's it. 

## References 

The ideas described above have been developed by various researchers in computer science, statistics, and applied math (see, for example, the series of papers by Arora, Ge, and collaborators). The specific approach taken here is based on [this thesis](https://www.sfu.ca/content/dam/sfu/stat-actsci/Graduate/Phelan-Gabriel-MSc-Project.pdf), which includes references to the requisite literature. Note however, that our approach has evolved since the thesis at the above link was written. New developments will be detailed in an upcoming paper. 

## See also 

An older package, `inferNMF`, more accurately reflects the original thesis work. Find that package [here](https://gitlab.com/gabrielp19/infernmf), though note that it hasn't been updated or tested in a while. 